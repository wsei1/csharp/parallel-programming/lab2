﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LAB2
{
	class Program
	{
		static void Main(string[] args)
		{
			List<IFunction> Functions = new List<IFunction>
			{
				new Function1(),
				new Function2(),
				new Function3()
			};

			Console.WriteLine("Function list: ");
			foreach (IFunction functionData in Functions)
			{
				Console.WriteLine($"Id: {functionData.Id}, Name: {functionData.Name}");
			}

			var choice = int.Parse(Console.ReadLine());

			if (!Functions.Any(x => x.Id == choice))
			{
				Console.WriteLine("You picked wrong number.");
			}
			else
			{
				var choosedFunction = Functions.FirstOrDefault(func => func.Id == choice);
				Worker worker = new Worker(choosedFunction);
				worker.Run();
			}

			Console.ReadKey();
		}
	}
}