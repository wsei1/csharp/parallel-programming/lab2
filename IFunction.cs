﻿namespace LAB2
{
	public interface IFunction
	{
		int Id { get; }
		string Name { get; }
		decimal GetY(decimal x);
	}
}
